package ee.eek.eduard.seregin.telegram.botalobot;

import ee.eek.eduard.seregin.telegram.botalobot.input_output.FactorySessionTelegram;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.IoTelegram;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.OiTelegram;

public class Main {
	
	public static void main(String[] args) {

	FactorySessionTelegram.instance(IoTelegram.IO, OiTelegram.instance());
	//FactorySessionTelegramSecond.instance(IoConsole.IO, OiConsole.instance());
	IoTelegram.IO.listen();
	
    }
}
	