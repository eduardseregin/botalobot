package ee.eek.eduard.seregin.telegram.botalobot.service.entities;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.strategies.Strategy;

public abstract class Entity {

	private static List <Entity> existingEntities = new ArrayList<>();
	protected Type type;
	protected Date nextUpdate;
	
	public static Entity instance (Type type) {
		existingEntities = existingEntities.stream().filter(x->new Date ().compareTo(x.nextUpdate)<0).collect(Collectors.toList());
		if (existingEntities.stream().anyMatch(x->x.type.equals(type))) {
			return existingEntities.stream().filter(x->x.type.equals(type)).findFirst().orElse(null); // null checked and will not appear
		}
		return type.getEntity();
	}
	
	protected Entity(Type type) {
	  this.type=type;
	  this.nextUpdate=new Date (System.currentTimeMillis() + getLivePeriodMin ()*60*1000);
	  existingEntities.add(this);
	}
	
	public final OutMessage getMessage(InMessage msg, Strategy strategy) {
		return strategy.compileMessage(getDto (), msg);
	}
	
	abstract ServiceDto getDto ();
	abstract int getLivePeriodMin ();
}
