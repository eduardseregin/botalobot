package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage.Html;
import ee.eek.eduard.seregin.telegram.botalobot.keyboards.KeyBoards;

public class SessionStandard extends Session implements Observer{

	
	public SessionStandard(long chatId, Oi sendDevice) {
		super(chatId, sendDevice);
		sendMsg (new OutMessage (chatId, "/Start", Html.Text));
	}

	@Override
	public void notifySubscribers(InMessage msg) {
		 observers.stream().forEach(o->o.update(msg));
	}

	@Override
	public void update(InMessage msg) {
		notifySubscribers (msg);
	}

	@Override
	public void sendMsg(OutMessage msg) {
		msg.setKeyboard(KeyBoards.MY_KEYBOARD.getKeyBoard());
		sendDevice.sendMessage(msg);
	}

	
}
