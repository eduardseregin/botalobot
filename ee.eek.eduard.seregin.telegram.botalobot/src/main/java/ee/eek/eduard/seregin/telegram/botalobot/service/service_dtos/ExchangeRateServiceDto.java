package ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;

@Data
public class ExchangeRateServiceDto extends ServiceDto {

	private Map <String, Double> exchangeRates;
	private String establishedDate;
	
	public ExchangeRateServiceDto() {
		this.exchangeRates = new HashMap<>();
	}
	
	
}
