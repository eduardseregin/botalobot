package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage;

public abstract class Oi {

	abstract void sendMessage (OutMessage msg);
}
