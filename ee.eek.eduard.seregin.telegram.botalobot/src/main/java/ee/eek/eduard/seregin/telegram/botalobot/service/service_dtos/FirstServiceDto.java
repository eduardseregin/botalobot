package ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos;

import lombok.Data;

@Data
public class FirstServiceDto extends ServiceDto {
	private int tempLow;
	private int tempHigh;	
}
