package ee.eek.eduard.seregin.telegram.botalobot.service.strategies.assisting;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.TemperatureServiceDto;

public class TemperatureCaseAssisting {

	private TemperatureServiceDto dto;
	
	public TemperatureCaseAssisting (TemperatureServiceDto dto) {
		this.dto = dto;
		
	}
	
	public String snowMsg() {
		String string = "";
		if (!dto.getSnow().contains(true))
			string += "";
		else {
			string += snowSign(dto.getSnow().indexOf(true) + 1);
			string += "\n";
		}
		
		return string;
	}

	public String rainMsg () {
		String string = "";
		if (!dto.getRain().contains(true))
			string += rainSign(0);
		else
			string += rainSign(dto.getRain().indexOf(true) + 1);
		string += "\n";
		
		return string;
	}
	
	public String currentTempMsg () {
		String string="";

		string+="Сейчас" + celciusSign (dto.getTempNow());
		string+="\n";
		
		return string;
	}
	
	public String nextHoursTempMsg () {
		String string="";
		
		if (dto.getMinToday()==dto.getMaxToday()) {
		
			if (dto.getMaxToday()==dto.getTempNow()) string+="В ближайшие часы температура не изменится";
			else string+="В ближайшие часы" + celciusSign (dto.getMaxToday());

		}
		
		else 
			string+="В ближайшие часы температура от " + dto.getMinToday() + " до " + dto.getMaxToday() + " градусов";
		
		return string;
	}
	
	
	public String wholeDayTempMsg () {
		String string="";
		
		if (this.dto.getTempHourly ().size()>5) {
			
			int min = Collections.min (this.dto.getTempHourly ().stream().limit(5).collect(Collectors.toList()));
			int max = Collections.max (this.dto.getTempHourly ().stream().limit(5).collect(Collectors.toList()));
			int minD = Collections.min(this.dto.getTempHourly ());
			int maxD = Collections.max(this.dto.getTempHourly ());
			
			if (minD!=maxD) {
				
				if (min==minD && max==maxD) string+="\n" + "В течение дня температура не изменится";
				else string+="\n" + "В течение дня температура от " + minD + " до " + maxD + " градусов";	
				
			}
		}
		
		return string;
	}
	
	public String weekRainMsg () {
		
		 if (dto.getRainWeek().stream ().anyMatch(x->x==true)) {
			
				
			String message = "\n";
			message+="Дождь ожидается в ";
			
			message = IntStream.range(0, 7)
			  .filter(x->dto.getRainWeek().get (x)==true)
			           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek ())))			
			               .reduce (message, (remainer, increm) -> remainer+increm+", ");
			
			if (message.lastIndexOf(",")>0) {
					message = message.substring(0,message.lastIndexOf(","));
			}
			return message + ".\n";
			
		} else {
			return "\n"+"Дождь на неделе не предвидится." + "\n";
		}
		
	}

	private static String getDayOfWeek (int day)  {
		try {
		if (day<0) throw new Exception("Day of the week is wrongly defined");
		}
		catch (Exception e) {System.out.println(e);}
		
		day = day%7;
		
		List <String> days = Arrays.asList("понедельник","вторник", "среду", "четверг", "пятницу", "субботу", "воскресенье");
		return days.get(day);
	}
	
	public String weekTempHighMsg () {
		if (dto.getHighTempWeek().stream ().anyMatch(x->x>dto.getMaxToday())) {
	
			String message = "\n";
			
			int median = getMedian (dto.getHighTempWeek().stream ().filter (x->x>dto.getMaxToday()).collect(Collectors.toList()));
			
			if (median == 0) {
			
			  message+="Повышение температуры ожидается в ";
			
			  message = IntStream.range(0, 7)
			      .filter(x->dto.getHighTempWeek().get (x)>dto.getMaxToday())
			           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek ())) + " (" + dto.getHighTempWeek().get (x) + new String(Character.toChars(0x00B0))+")")			
			               .reduce (message, (remainer, increm) -> remainer+increm+", ");
			
			  if (message.lastIndexOf(",")>0) {
					message = message.substring(0,message.lastIndexOf(","));
			}
			} else {
				
				message += "На неделе температура повысится до " + median + new String(Character.toChars(0x00B1)) + "2"+ new String(Character.toChars(0x00B0)) + ".\n";
				
				if (dto.getHighTempWeek().stream ().anyMatch(x->x>median+2)) {
				  message+="Более высокая температура ожидается в ";
				
				  message = IntStream.range(0, 7)
					      .filter(x->dto.getHighTempWeek().get (x)>median+2)
					           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek ())) + " (" + dto.getHighTempWeek().get (x) + new String(Character.toChars(0x00B0))+")")			
					               .reduce (message, (remainer, increm) -> remainer+increm+", ");
				  if (message.lastIndexOf(",")>0) {
						message = message.substring(0,message.lastIndexOf(","));
				}
				  message = message+".";
				}
			}
			
			
			return message;
			
		} 
			return "Более высокая температура на неделе не предвидится." + "\n";
		
	}
	
	private int thisDayOfWeek () {
		return GregorianCalendar.getInstance().get(Calendar.DAY_OF_WEEK) - 
				GregorianCalendar.getInstance().getFirstDayOfWeek()+1;
	}
	
	public String weekTempLowMsg () {
		
		if (dto.getLowTempWeek().stream ().anyMatch(x->x<dto.getMinToday())) {
	
			String message = "\n";
			
			int median = getMedian (dto.getLowTempWeek().stream ().filter (x->x<dto.getMinToday()).collect(Collectors.toList()));
			
			if (median == 0) {
			
			  message+="Понижение температуры ожидается в ";
			
			  message = IntStream.range(0, 7)
			      .filter(x->dto.getLowTempWeek().get (x)<dto.getMinToday())
			           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek ())) + " (" + dto.getLowTempWeek().get (x) + new String(Character.toChars(0x00B0))+")")			
			               .reduce (message, (remainer, increm) -> remainer+increm+", ");
			
			  if (message.lastIndexOf(",")>0) {
					message = message.substring(0,message.lastIndexOf(","));
			}
			} else {
				
				message += "На неделе ночью температура понизится до " + median + new String(Character.toChars(0x00B1)) + "2"+ new String(Character.toChars(0x00B0)) + ".\n";
				
				if (dto.getLowTempWeek().stream ().anyMatch(x->x<median-2)) {
				  message+="Более низкая температура ожидается в ";
				
				  message = IntStream.range(0, 7)
					      .filter(x->dto.getLowTempWeek().get (x)<median-2)
					           .mapToObj (x->getDayOfWeek((x+thisDayOfWeek ())) + " (" + dto.getLowTempWeek().get (x) + new String(Character.toChars(0x00B0))+")")			
					               .reduce (message, (remainer, increm) -> remainer+increm+", ");
				  if (message.lastIndexOf(",")>0) {
						message = message.substring(0,message.lastIndexOf(","));
				}
				  message = message+".";
				}
			}
			
			
			return message;
			
		} 
			return "Более низкая температура на неделе не предвидится." + "\n";
		
	}
	
	
	private int getMedian (List <Integer> list) {
		//gets the temp where +/-1 grad locates the most days.
		
		if (list == null) return 0;
		if (list.size()<3) return 0;
		
		int maxCount =0;
		int median = 0;
		Set <Integer> countSet = list.stream().collect(Collectors.toSet());
		
		for (int temp : countSet) {
			int cnt = (int) list.stream().mapToInt(x->x).filter (x->x>=temp-2 && x<=temp+2).count();
			if (maxCount<cnt) {
				median = temp; 
				maxCount =  cnt;
			}
		}
		
		if (maxCount>=list.size()/2) return median;
		
		return 0;
		
	}
	
	private String rainSign (int hours) {
		if (hours == 0) return "Дождя нет";
		if (hours == 1) return "Дождь в ближайший час";
		if (hours<=4) return "Дождь в ближайшие " + hours +" часа";
		return "Дождь в ближайшие " + hours +" часов";
	}
	
	private String snowSign(int hours) {
		if (hours == 0) return "Снега нет";
		if (hours == 1) return "Снег в ближайший час";
		if (hours<=4) return "Снег в ближайшие " + hours +" часа";
		return "Снег в ближайшие " + hours +" часов";
	}

	
	private String celciusSign (int gradus) {
		if (Math.abs(gradus)%10==1 && Math.abs(gradus)!=11) return " " +gradus + " градус";
		if (Math.abs(gradus)%10>1 && Math.abs(gradus)%10<=4 && Math.abs(gradus)!=12 && Math.abs(gradus)!=13 && Math.abs(gradus)!=14) 
			return " " + gradus + " градуса";
		return " " + gradus + " градусов";
	}
	
}
