package ee.eek.eduard.seregin.telegram.botalobot.service.entities;

public interface Type {

	Entity getEntity ();
	
}
