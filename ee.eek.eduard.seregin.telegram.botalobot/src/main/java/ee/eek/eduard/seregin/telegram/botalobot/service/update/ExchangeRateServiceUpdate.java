package ee.eek.eduard.seregin.telegram.botalobot.service.update;

import java.util.List;

import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ExchangeRateServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.utilities.Parser;

public class ExchangeRateServiceUpdate implements ServiceUpdate {

	private ExchangeRateServiceDto dto;
	
	public ExchangeRateServiceUpdate() {
		this.dto = new ExchangeRateServiceDto ();
	}

	@Override
	public ExchangeRateServiceDto getUpdate () {
		download ();
		return this.dto;
	}
	
	
private void download () {
	   List <String> lines = new Parser.Builder("Exchange Rates")
	    		.url("https://cbr.ru/currency_base/daily/")
	    		.elem("data")
	    		.build().getElements();

if (lines!=null) {
	   // prepares the line and Map
	    String cbRFRates = lines.get(0);
	    cbRFRates=cbRFRates.substring(posEnglCapitalLetterFirst (cbRFRates));
	    
	    // fills the map with data
	    while (posEnglCapitalLetterFirst (cbRFRates)!=0) {

	    	String individualLine = cbRFRates.substring(0, posEnglCapitalLetterFirst (cbRFRates)-1);
	    	
	    	dto.getExchangeRates().put(individualLine.substring(0, 3), extractRate(individualLine));
	    	cbRFRates=cbRFRates.substring(posEnglCapitalLetterFirst (cbRFRates));
	   }
	   // adds-up the last line of the inf 
	    
	    if (cbRFRates.length()!=0) dto.getExchangeRates().put(cbRFRates.substring(0, 3), extractRate(cbRFRates+" 1"));
		// DONE    
}

       dto.setEstablishedDate(getEstablishedDate());
	    
}

private int posEnglCapitalLetterFirst(String r) {

	int pos = r.length() - 1;

	for (int i = 3; i < r.length(); i++) {
		if (pos == r.length() - 1) {
			for (int y = 65; y < 91; y++) {
				if (r.charAt(i) == ((char) (y))) {
					pos = i;
					break;
				}
			}
		}
	}

	if (pos != r.length() - 1)
		return pos;

	return 0;
}

private double extractRate(String s) {

	String rate = s.substring(0, s.lastIndexOf(" ") - 1);
	rate = rate.substring(rate.lastIndexOf(" ") + 1);
	rate = rate.replace(',', '.');

	String qtyString = s.substring(s.indexOf(" ") + 1, s.indexOf(" ", s.indexOf(" ") + 1));

	int qty = 1;

	try {
		qty = Integer.parseInt(qtyString);
	} catch (Exception e) {
	}

	try {
		int increasedDouble = (int) (Math.round(Double.parseDouble(rate) * 100 / qty));
		return (double) increasedDouble / 100;

	} catch (Exception e) {
		return 0;
	}
}

private static String getEstablishedDate () {
	
    List <String> date = new Parser.Builder("for Date")
    		.url("https://cbr.ru/currency_base/daily/")
    		.elem("datepicker-filter")
    		.attr("data-default-value")
    		.build().getElements();
    
    if (date!=null)
    return date.get(0);
    return "";
}
	
}
