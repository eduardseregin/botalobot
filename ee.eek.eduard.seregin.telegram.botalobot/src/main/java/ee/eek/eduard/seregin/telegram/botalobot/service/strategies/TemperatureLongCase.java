package ee.eek.eduard.seregin.telegram.botalobot.service.strategies;

import java.util.List;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage.Html;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.TemperatureServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.strategies.assisting.TemperatureCaseAssisting;

public class TemperatureLongCase extends Strategy {

	public TemperatureLongCase () {}
	
	@Override
	protected StringBuilder buildString(ServiceDto dto, InMessage msg) {
		TemperatureServiceDto dtoTS = (TemperatureServiceDto) dto;
		TemperatureCaseAssisting assistant = new  TemperatureCaseAssisting (dtoTS);
		StringBuilder message = new StringBuilder ();
		message.append ("Погода в " + cityByIndex(dtoTS.getCityIndex()) + ":" + "\n");
		message.append (assistant.rainMsg ());
		message.append (assistant.currentTempMsg ());
		message.append (assistant.nextHoursTempMsg ());
		message.append (assistant.wholeDayTempMsg ());
		message.append (assistant.weekRainMsg ());
		message.append (assistant.weekTempHighMsg ());
		message.append (assistant.weekTempLowMsg ());
		message.append (authorRef ());
		return message;
	}

	private String authorRef () {
		return "\n"+" /по данным сайта www.accuweather.com/"+"\n";
	}
	
	@Override
	protected Html isHtml() {
		return Html.Text;
	}

	public String cityByIndex (int index) {
		List <String> cities = List.of ("_Москве","Москве", "Калуге", "Таллине", "Санкт-Петербурге", "Твери",
					"Иркутске", "Тарту", "Пярну", "Екатеринбурге", "Липецке", "Воронеже", "Ростове", "Краснодаре", "Геленджике",
					"Сочи", "Анапе", "Минске", "Йихви", "Нарве", "Туле", "Владимире", 
					"Н.Новгороде", "Новгороде", "Киеве",
					"Тагиле", "Стамбуле", "Смоленске", "Пскове", "Ярославле", "Тбиллиси", "Ереване", "Перми", "Челябинске",
					"Тегеране", "Токио", "Симферополе", "Севастополе", "Будапеште", "Праге", "Вене", "Варне", "Бухаресте",
					"Софии", "Подгорице", "Лондоне", "Берлине", "Мюнхене", "Париже", "Хельсинки", "Стокгольме", "Осло","Коппенгагине",
					"Вильнюсе", "Риге", "Майкопе", "Армавире", "Ставраполе", "Тамбове", "Волгограде", "Саратове", "Филадельфии", "Ялте", 
					"Алуште", "Феодосии", "Евпатории", "Анкаре", "Судаке", "Джанкое", "Черноморском",
					"Керчи", "Тамани", "Анталье", "Риме", "Неаполе", "Милане", "Турине", "Венеции", "Афинах", "Никосии",
					"Лимассоле", "Петрозаводске", "Нью-Йорке", "Вашингтоне", "Балтиморе", "Ижевске", "Казани",
					"Новотроицке", "Оренбурге", "Уфе", "Кирове","Череповце", "Петропавловске-Камчатском",
					"Тихвине", "Самаре", "Белгороде", "Старом Осколе", "Астрахани","Архангельске", "Бургасе");
		
		return cities.get(index);
		
	}
	
}
