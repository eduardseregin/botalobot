package ee.eek.eduard.seregin.telegram.botalobot.input_output;


import java.util.ArrayList;
import java.util.List;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;


public abstract class FactorySession implements Observer {

	protected Io io;
	protected Oi oi;
	protected List <Session> sessions;

	protected FactorySession (Io ioInput, Oi oiOutput) {
		io=ioInput;
		oi = oiOutput;
		io.subscribeFactory(this);
		sessions = new ArrayList<>();
	}
	
	@Override
	public void update(InMessage msg) {

		if (sessions.stream().noneMatch(x->x.getChatId()==msg.getChatId())) {
			Session newSession = getSession(msg);
			io.subscribe(newSession);
			sessions.add(newSession);
			subscribeServicesToSession (newSession);
		}		
	}
	
	abstract Session getSession (InMessage msg);
	abstract void subscribeServicesToSession (Session session);
}
