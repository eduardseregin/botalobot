package ee.eek.eduard.seregin.telegram.botalobot.service.update;

import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ServiceDto;

public interface ServiceUpdate {

	ServiceDto getUpdate ();
	
}
