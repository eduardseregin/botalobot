package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;

public interface Observer {

	public void update (InMessage msg);
	
}
