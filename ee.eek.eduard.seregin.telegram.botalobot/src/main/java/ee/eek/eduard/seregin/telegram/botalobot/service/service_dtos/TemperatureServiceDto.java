package ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

@Data
public class TemperatureServiceDto extends ServiceDto {

	private Date today;
	private int cityIndex;
	private int tempNow;
	private int maxToday;
	private int minToday;
	private List <Integer> tempHourly;
	private List <Boolean> rain;
	private List <Boolean> snow;
	
	private List <Integer> highTempWeek;
	private List <Integer> lowTempWeek;
	private List <Boolean> rainWeek;
	
	public TemperatureServiceDto() {
		this.today = new Date ();
		this.tempHourly = new ArrayList<>();
		this.rain = new ArrayList<>();
		this.snow = new ArrayList<>();
		this.highTempWeek = new ArrayList<>();
		this.lowTempWeek = new ArrayList<>();
		this.rainWeek = new ArrayList<>();
	}
	
	
}
