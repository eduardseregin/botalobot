package ee.eek.eduard.seregin.telegram.botalobot.service.boundaries;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.Observer;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.Session;
import ee.eek.eduard.seregin.telegram.botalobot.service.entities.Entity;
import ee.eek.eduard.seregin.telegram.botalobot.service.entities.EntityTemperature.TypeCityTemperature;
import ee.eek.eduard.seregin.telegram.botalobot.service.strategies.Strategy;
import ee.eek.eduard.seregin.telegram.botalobot.service.strategies.TemperatureLongCase;

public class ServiceTemperature extends ServiceFactory implements Observer {

	private TypeCityTemperature type;
	
	public ServiceTemperature(Session session) {
		super(session);
		this.type = TypeCityTemperature.Moscow;
	}


	@Override
	protected boolean isServiceCall(InMessage msg) {
	
	String city = msg.getText().toLowerCase();	
    for (TypeCityTemperature tCity: TypeCityTemperature.values()) {
    	String t = tCity.toString().toLowerCase();
    	if (city.contains(t)) {
    		type = tCity;
    		return true;
    	}
    }
		return false;
	}

	@Override
	Strategy getStrategy(InMessage msg) {
		return new TemperatureLongCase();
	}

	@Override
	Entity createEntity() {
		return Entity.instance(this.type);
	}


}
