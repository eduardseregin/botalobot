package ee.eek.eduard.seregin.telegram.botalobot.keyboards;


import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import lombok.Getter;
import lombok.NonNull;


public enum KeyBoards {
	
	REMOVE (0),
	MY_KEYBOARD (1);

	private static List <KeyBoard> keyBoards = initialiseKeys (); 
	
	private int index;

	private KeyBoards(int index) {
		 this.index = index;
		 		
		if (index<0) {
			throw new IllegalArgumentException ("index should be positive");
		}
		
	}

	public String [] [] getKeyBoardStrings () {
		
		if (keyBoards.get(this.index)==null) return null;
		
		return keyBoards.get(this.index).getKeyBoard();
	}
	
	public KeyBoard getKeyBoard () {
		return keyBoards.get(this.index);
	}
	
	private static List<KeyBoard> initialiseKeys() {
		List <KeyBoard> boards = new ArrayList<>();
		
		boards.add(new KeyBoard.BoardBuilder(KeyBoards.REMOVE).buildBoard());
		
		boards.add(
				new KeyBoard.BoardBuilder(KeyBoards.MY_KEYBOARD)
				    .add (new Key.Builder ("Rates").emojiString("20AC").level(Level.UP).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Burgas").emojiString("1F324").level(Level.UP).build())
				            .add (new Key.Builder ("Sofia").level(Level.UP).build())
				    
				    .add (new Key.Builder ("Varna").emojiString("1F324").level(Level.MIDDLE).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Istanbul").emojiString("1F306").level(Level.MIDDLE).build())
				        .add (new Key.Builder ("Gyelyendzhik").level(Level.MIDDLE).build())
				        .add (new Key.Builder ("Sevastopol").level(Level.MIDDLE).build())
				    
				    .add (new Key.Builder ("Moscow").emojiString("1F3D9").level(Level.DOWN).priority(Priority.HIGH).build())
				        .add (new Key.Builder ("Kaluga").level(Level.DOWN).build())
				        .add (new Key.Builder ("Ekaterinburg").emojiString("1F3D4").level(Level.DOWN).build())
				
				.buildBoard()
				);
		
			
				
		return boards;
	}
	
	private static class Key implements Comparable <Key> {

		@NonNull
		private String  keyName;
		private String emojiString;
		private Level level;
		private Priority priority;
		
		
		public static class Builder {
			
			private String keyName;
			private String emojiString;
			private Level level;
			private Priority priority;

			
			public Builder(@NonNull String keyName) {
				keyName = keyName.trim();
				if (!Objects.nonNull(keyName)) {
					throw new IllegalArgumentException ("@keyName should not be null");
				}
				if (keyName.length()==0) {
					throw new IllegalArgumentException ("@keyName should not be empty");
				}
				
				this.keyName = keyName;
				this.emojiString = "";
				this.level = Level.DOWN;
				this.priority = Priority.NORMAL;
			}
			
			public Builder emojiString (String emoji) {
				if (emoji!=null) {
				  emoji = emoji.trim();
				  this.emojiString = emoji;
				}
				return this;
			}
			
			public Builder level (Level val) {
				if (val!=null) {
				  this.level=val;
				}
				return this;
			}
			
			public Builder priority (Priority val) {
				if (val!=null) {
				  this.priority=val;
				}
				return this;
			}
			
			public Key build () {
				return new Key (this);
			}
			
			
		}
		
		
		
		private Key(Builder builder) {
		
			this.keyName = builder.keyName;
			this.emojiString = builder.emojiString;
			this.level = builder.level;
			this.priority = builder.priority;
		}


		
		
		public Level getLevel() {
			return level;
		}




		public String getKey () {
			if (this.emojiString!=null && this.emojiString.length()>1) {
				String space = "";
				if (keyName.length()>11) {
					space = "  ";
				} else {
					space = "   ";
				}
				return keyName + space + new String(Character.toChars(Integer.decode("0x"+emojiString)));
			}
			
			return keyName;
		}
		
		@Override
		public String toString() {
			return "Key [keyName=" + keyName + ", emojiString=" + emojiString + ", level=" + level + ", priority="
					+ priority + "]";
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((emojiString == null) ? 0 : emojiString.hashCode());
			result = prime * result + ((keyName == null) ? 0 : keyName.hashCode());
			result = prime * result + ((level == null) ? 0 : level.hashCode());
			result = prime * result + ((priority == null) ? 0 : priority.hashCode());
			return result;
		}


	@Override
	public int compareTo (Key other) {
		
		if (this.level == other.level && this.priority == other.priority) {
			return 0;
		}
		
		if (this.level==Level.UP && this.priority==Priority.HIGH) {
			return -1;
		}
		if (this.level == Level.UP && other.level!=Level.UP) {
			return -1;
		}
		if (this.level == Level.MIDDLE && other.level==Level.DOWN) {
			return -1;
		}
		
		if (this.level == other.level && this.priority==Priority.HIGH) {
			return -1;
		}
		
		return 1;

	}

	
	}
	
	public static class KeyBoard {
		
		@Getter
		private KeyBoards name;
		private List <Key> keys;
		
		
		public static class BoardBuilder {
			
			private KeyBoards name;
			private List <Key> keys;
			
			public BoardBuilder(KeyBoards name) {
				this.name = name;
				this.keys = new ArrayList<>();
			}
			
			public BoardBuilder add (Key keyMaxNine) {
				
				if (keys.size()==12) {
					throw new IllegalArgumentException ("Only 12 keys are allowed to add");
				}
				
				if ((keys.stream().filter(x->x.getLevel()==Level.UP).count()==4 
				    && keyMaxNine.getLevel() == Level.UP) ||
				        (keys.stream().filter(x->x.getLevel()==Level.MIDDLE).count()==4 
				            && keyMaxNine.getLevel() == Level.MIDDLE)||
				                (keys.stream().filter(x->x.getLevel()==Level.DOWN).count()==4) 
				                    && keyMaxNine.getLevel() == Level.DOWN) {
		
					throw new IllegalArgumentException ("Only 4 keys are allowed in any given level");
				}
				
				if (keyMaxNine!=null) {
				keys.add(keyMaxNine);
				}
				return this;
			}
			
			public KeyBoard buildBoard () {

			    this.keys = this.keys.stream().limit(12).sorted().collect(Collectors.toList());
				List <Key> toAdd = keys.stream().filter(x->x.getLevel()==Level.UP).limit (4).collect (Collectors.toList()); 
				toAdd.addAll(keys.stream().filter(x->x.getLevel()==Level.MIDDLE).limit (4).collect (Collectors.toList()));
				toAdd.addAll(keys.stream().filter(x->x.getLevel()==Level.DOWN).limit (4).collect (Collectors.toList()));
				
				
				this.keys = toAdd;
						
				return new KeyBoard (this);
			}
		}
		
		
		public KeyBoard(BoardBuilder builder) {
		   this.name = builder.name;
		   this.keys = builder.keys;
		}


		public String [] [] getKeyBoard () {
			
			int rows = (int) keys.stream().map(x->x.getLevel()).distinct().count();		
			
			String [] [] array = new String [rows] [];
			
			int count = 0;
			
			for (Level l: Level.values()) {
				
				if (keys.stream().anyMatch(x->x.getLevel()==l)) {
					  List <Key> ofLevel = keys.stream().filter (x->x.getLevel()==l).sorted().collect(Collectors.toList());
					  array [count] = ofLevel.stream().map (x->x.getKey()).toArray(String[]::new);
					  count++;
					} 
			}
			
			
			return array;
		}
		
		@Override
		public String toString() {
			String msg = "KeyBoard [name=" + name + ", keys: " + "\n";
			return keys.stream().map(x->x.toString()).reduce(msg,  (x,y)->""+x+y+"\n");
		}
		
		
	}
}

enum Level {UP, MIDDLE, DOWN}

enum Priority {HIGH, NORMAL}

