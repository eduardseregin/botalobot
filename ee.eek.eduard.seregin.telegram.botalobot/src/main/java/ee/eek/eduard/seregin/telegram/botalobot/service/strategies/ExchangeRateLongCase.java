package ee.eek.eduard.seregin.telegram.botalobot.service.strategies;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage.Html;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ExchangeRateServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ServiceDto;

public class ExchangeRateLongCase extends Strategy {

	public ExchangeRateLongCase () {}
	
	@Override
	protected StringBuilder buildString(ServiceDto dto, InMessage msg) {
		ExchangeRateServiceDto dtoER = (ExchangeRateServiceDto) dto;
		StringBuilder message = new StringBuilder ();
		if (dtoER.getExchangeRates()!=null) {
		message.append ("Банком России (cbr.ru) установлены курсы на " + dtoER.getEstablishedDate() + "\n");
		message.append ("  EUR - " + dtoER.getExchangeRates().get("EUR") + "руб."+ "\n");
		message.append ("  GBP - " + dtoER.getExchangeRates().get("GBP") + "руб."+ "\n");
		message.append ("  USD - " + dtoER.getExchangeRates().get("USD") + "руб."+ "\n");
		message.append ("  BGN - " + dtoER.getExchangeRates().get("BGN") + "руб."+ "\n");
		} else {
		message.append ("Информация недоступна");
		}	
			
		return message;
	}

	
	@Override
	protected Html isHtml() {
		return Html.Text;
	}

}
