package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.Message;
import com.pengrad.telegrambot.model.Update;
import com.pengrad.telegrambot.request.GetUpdates;
import com.pengrad.telegrambot.response.GetUpdatesResponse;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;

public class IoTelegram extends Io {

  public final static Io IO = new IoTelegram ();

  private final static TelegramBot bot = new TelegramBot (Credentials.getTelegramCred());
  private static int lastUpdateId = 97483080;
  
  private IoTelegram() {
	  System.out.println("telegram receiving messages started");
  }

  @Override
  protected InMessage receiveMsg() {
     Message receivedMsg = telegramInput();
    
    if (receivedMsg==null) return null;
    InMessage msg = new InMessage ();
    msg.setChatId(receivedMsg.chat().id());
    msg.setText(receivedMsg.text());
	msg.setDate(new Date ());
  return msg;
  }

   private Message telegramInput() {
	   GetUpdates getUpdates = new GetUpdates().limit(200).offset(lastUpdateId).timeout(0);
	   
	   GetUpdatesResponse updatesResponse = bot.execute(getUpdates);
		List<Update> updates = updatesResponse.updates();
		if (updates==null) updates = new ArrayList<>();
		
		updates = updates.stream().filter(x->x.message().text()!=null).collect(Collectors.toList());
		
		int minUpdateId=updates.stream().map(x->x.updateId()).mapToInt(x->x).min().orElse(0);
		
		if (minUpdateId==0) return null;
		
		Update update = updates.stream().filter(x->x.updateId()==minUpdateId).findFirst().orElse(null);
		if (lastUpdateId<=update.updateId()) lastUpdateId=update.updateId()+1;
		System.out.println(update.message().chat().id() + ":  "+lastUpdateId + " : " + update.message().text());
		
		return update.message();	
  
}
   
 private static class Credentials {
		  static String getTelegramCred() {
			  return "1719714699:AAGk351or7a3AqhJ8yxD8KgdFYhhyeq06MQ";  
		  }
}
   
}
   
 
