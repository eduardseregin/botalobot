package ee.eek.eduard.seregin.telegram.botalobot.service.update;

import java.util.Calendar;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.stream.Collectors;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import ee.eek.eduard.seregin.telegram.botalobot.service.entities.EntityTemperature.TypeCityTemperature;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.TemperatureServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.utilities.Parser;

public class TemperatureServiceUpdate implements ServiceUpdate {

	private TemperatureServiceDto dto;
	private String cityRef;
	private String endRef;
	
	public TemperatureServiceUpdate(TypeCityTemperature type) {
		this.dto = new TemperatureServiceDto ();
		this.dto.setCityIndex(type.getIndex());
		this.cityRef = type.getRef();
		this.endRef = getEndRef(cityRef);
		
	}

	@Override
	public TemperatureServiceDto getUpdate () {
		obtainCurrentWeather ();
		obtainWeekForecast ();
		setMinMax();
		return this.dto;
	}
	
	
	
private String getEndRef (String cityRef) {
	return cityRef.substring(cityRef.lastIndexOf("/")+1);
}
	private void obtainCurrentWeather () {
		
		try {
			
			 List <Integer> tempsNow = new Parser.Builder("Temperature that is now")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/weather-forecast/"+endRef)
			    		.elem("recent-location__temp")
			    		.build().toIntList();
			 if (tempsNow!=null) if (tempsNow.size()>0) dto.setTempNow(tempsNow.get(0)); 
			    
			    
			 List <Integer>  tempHourly = new Parser.Builder("Temperature for hourly temperature")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/hourly-weather-forecast/"+endRef)
			    		.elem("temp metric")
			    		.build().toIntList();
			 
			 dto.setTempHourly(tempHourly);
			    
			    List <Integer> weathersCurrent = new Parser.Builder("Weather for search of rain NOW")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/weather-forecast/"+endRef)
			    		.elem("header-weather-icon")
			    		.div(".svg")
			    		.attr("src")
			    		.build().toIntList();
			    
			 		    
			    if (weathersCurrent.get(0)>=12 && weathersCurrent.get(0)<=15 ) dto.getRain().add(true);
		    	else dto.getRain().add(false);
			    
			    if (weathersCurrent.get(0)==19 || weathersCurrent.get(0)==26 || weathersCurrent.get(0)==29  ) dto.getSnow().add(true);
		    	else dto.getSnow().add(false);
			    
			    List <Integer> weathers = new Parser.Builder("Weather for search of rain")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/hourly-weather-forecast/"+endRef)
			    		.elem("weather-icon icon")
			    		.div(".svg")
			    		.attr("data-src")
			    		.build().toIntList();
			    
			    			    
		    
			    for (int i=0; i<weathers.size(); i++) {
			    	if (weathers.get(i)>=12 && weathers.get(i)<=15 ) dto.getRain().add(true);
			    	else dto.getRain().add(false);
			    	
			    	if (weathers.get(i)==19 || weathers.get(i)==26 || weathers.get(i)==29  ) dto.getSnow().add(true);
			    	else dto.getSnow().add(false);
			    }
		
		} catch (Exception e) {}
		

	}
	
	private void obtainWeekForecast () {
		
	
		try {
		
			List <Integer> highTempWeek = new Parser.Builder("Temperature for days of the week high temperature")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef)
			    		.elem("high")
			    		.build().toIntList();
			
			
			List <Integer> lowTempWeek = new Parser.Builder("Temperature for days of the week low  temperature")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef)
			    		.elem("low")
			    		.build().toIntList();
		
			
			 List <Integer> rainWeekInt = new Parser.Builder("Weather for search of rain during the week")
			    		.url("https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef)
			    		.elem("weather-icon icon")
			    		.div(".svg")
			    		.attr("data-src")
			    		.build().toIntList();
			 
			 // skips today, as this information is collected differently. Limits to the upcoming week days
			
			 List <Boolean> rainWeek;
			 
			 if (isFromToday (cityRef, endRef)) {
				 
			 rainWeek = rainWeekInt.stream().map(x->x>=12&&x<=15).skip(1).limit (7).collect(Collectors.toList());
			 highTempWeek = highTempWeek.stream().skip(1).limit (7).collect(Collectors.toList());
			 lowTempWeek = lowTempWeek.stream().skip(1).limit (7).collect(Collectors.toList());
			 } else {
			
				 //not skip (1) - as accuWeather already skipped the first day
			 
			 rainWeek = rainWeekInt.stream().map(x->x>=12&&x<=15).limit (7).collect(Collectors.toList());
			 highTempWeek = highTempWeek.stream().limit (7).collect(Collectors.toList());
			 lowTempWeek = lowTempWeek.stream().limit (7).collect(Collectors.toList());
			 }	
			 
			 dto.setHighTempWeek(highTempWeek);
			 dto.setLowTempWeek(lowTempWeek);
			 dto.setRainWeek(rainWeek);
			 
	} catch (Exception e) {}
		
	}

	private void setMinMax () {
		int maxToday = Collections.max (this.dto.getTempHourly ().stream().limit(5).collect(Collectors.toList()));
		int minToday = Collections.min (this.dto.getTempHourly ().stream().limit(5).collect(Collectors.toList()));
		
		dto.setMaxToday(maxToday);
		dto.setMinToday(minToday);
	}
	
	private boolean isFromToday (String cityRef, String endRef) {
		 final String url = "https://www.accuweather.com/ru/"+cityRef+"/daily-weather-forecast/"+endRef;
			
		 final String elem = "module-header sub date";
		 
		 boolean isFromToday = false;
		 
		 
			try {
				Document doc = Jsoup.connect(url).get();
				List <String> list= doc.getElementsByClass(elem).eachText();
			
				String d = list.get(0).substring(0,list.get(0).indexOf("."));
				int dateAccuWeek = Integer.parseInt(d);
				isFromToday = new GregorianCalendar ().get (Calendar.DATE)==dateAccuWeek;
			
			}
				catch (Exception e) {
					System.out.println("Testing of starting date in week forecast failed");
					}

			return isFromToday;
	}

	
}
