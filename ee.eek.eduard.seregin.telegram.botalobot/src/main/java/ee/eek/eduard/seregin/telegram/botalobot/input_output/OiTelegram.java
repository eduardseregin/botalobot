package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.model.request.Keyboard;
import com.pengrad.telegrambot.model.request.ParseMode;
import com.pengrad.telegrambot.model.request.ReplyKeyboardMarkup;
import com.pengrad.telegrambot.model.request.ReplyKeyboardRemove;
import com.pengrad.telegrambot.request.SendMessage;

import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage.Html;
import ee.eek.eduard.seregin.telegram.botalobot.keyboards.KeyBoards;

public class OiTelegram extends Oi {

	private final static TelegramBot bot = new TelegramBot (Credentials.getTelegramCred());
	private static OiTelegram oi;
	
	
	public static OiTelegram instance () {
		if (oi == null) oi = new OiTelegram ();
		return oi;
	}
	
	private OiTelegram () {}
	
	@Override
	void sendMessage(OutMessage msg) {
		
		SendMessage message = new SendMessage (msg.getChatId(), msg.getText());
		addMessageKeyboard (msg, message);
		addMessageHtml (msg.getIsHtml(), message);
		
		bot.execute(message);
			
	}

	private void addMessageHtml(Html html, SendMessage message) {
		if (html == Html.Html) message.parseMode(ParseMode.HTML);
		
	}

	private void addMessageKeyboard (OutMessage msg, SendMessage message) {

		Keyboard board = getKeyBoard(msg);
		message.replyMarkup(board);
	}	
	
	
	private Keyboard getKeyBoard(OutMessage msg) {
		KeyBoards type = msg.getKeyboard().getName();
		if (type == KeyBoards.REMOVE) return getRemoveKeyboard();
		
		return getNewKeyboard(msg.getKeyboard().getKeyBoard());
	}

	private Keyboard getNewKeyboard(String [] [] keys) {
		ReplyKeyboardMarkup board = new ReplyKeyboardMarkup (keys);
		board.oneTimeKeyboard(true); 
		board.resizeKeyboard(true);
		board.selective(false);
		return board;
	}
	
	private Keyboard getRemoveKeyboard() {
		Keyboard board = new ReplyKeyboardRemove ();
		return board;
	}
	
		 private static class Credentials {
			  static String getTelegramCred() {
				  return "1719714699:AAGk351or7a3AqhJ8yxD8KgdFYhhyeq06MQ";  
			  }
	}

}
