package ee.eek.eduard.seregin.telegram.botalobot.service.strategies;


import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage.Html;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ServiceDto;

public abstract class Strategy {

	public final OutMessage compileMessage (ServiceDto dto, InMessage msg) {
		return new OutMessage (msg, buildString (dto, msg).toString(), isHtml ());
	}
	
	abstract protected StringBuilder buildString (ServiceDto dto, InMessage msg);
	abstract protected Html isHtml ();
}
