package ee.eek.eduard.seregin.telegram.botalobot.service.boundaries;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.Observer;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.Session;
import ee.eek.eduard.seregin.telegram.botalobot.service.entities.Entity;
import ee.eek.eduard.seregin.telegram.botalobot.service.entities.EntityExchangeRate.TypeExchangeRate;
import ee.eek.eduard.seregin.telegram.botalobot.service.strategies.ExchangeRateLongCase;
import ee.eek.eduard.seregin.telegram.botalobot.service.strategies.Strategy;

public class ServiceExchangeRate extends ServiceFactory implements Observer {


	public ServiceExchangeRate(Session session) {
		super(session);
	}


	@Override
	protected boolean isServiceCall(InMessage msg) {
		String text = msg.getText().toLowerCase();
		if (text.contains("rate")) return true;
		return false;
	}

	@Override
	Strategy getStrategy(InMessage msg) {
		return new ExchangeRateLongCase();
	}

	@Override
	Entity createEntity() {
		return Entity.instance(TypeExchangeRate.CBR);
	}


}
