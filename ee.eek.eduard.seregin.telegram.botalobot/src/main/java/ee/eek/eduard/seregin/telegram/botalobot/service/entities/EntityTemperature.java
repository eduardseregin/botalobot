package ee.eek.eduard.seregin.telegram.botalobot.service.entities;


import java.util.List;

import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.TemperatureServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.update.TemperatureServiceUpdate;
import lombok.Getter;

public class EntityTemperature extends Entity {

	public final static int LIVE_PERIOD_MIN = 60;
	
	
	protected TemperatureServiceDto dto;
	
	
	protected EntityTemperature(TypeCityTemperature type) {
		super (type);
		this.dto = new TemperatureServiceUpdate(type).getUpdate();
	}

	@Override
	public ServiceDto getDto() {
		return dto;
	}

	public static enum TypeCityTemperature implements Type {
		Moscow (1), Kaluga (2), Tallinn (3), StP (4), Tver (5), Irkutsk (6), Tartu (7), Parnu (8), 
		Ekaterinburg (9), Lipetsk (10), Voronezh (11), Rostov (12), Krasnodar (13), Gyelyendzhik (14), Sochi (15), Anapa (16), 
		Minsk (17), Johv (18), Narva (19), Tula (20), Vladimir (21), NizhnyNovgorod (22), Novgorod (23), Kyiv (24), 
		NizhnyTagil (25),
		Istanbul (26), Smolensk (27), Pskov (28), Yaroslavl (29), Tbilisi (30), Yerevan (31), Perm (32), Chelyabinsk (33), 
		Tehran (34), Tokyo (35), Simferopol (36), Sevastopol (37), Budapest (38), Prague (39), Vienna (40), Varna (41), 
		Bucharest (42), Sofia (43), Podgorica (44), London (45), Berlin (46), Munich (47), Paris (48), Helsinki (49),
		Stockholm (50), Oslo (51), Copenhagen (52), Vilnus (53), Riga (54), Maykop (55), Armavir (56), Stavropol (57),
		Tambov (58), Volgograd (59), Saratov (60), Philadelpia (61), Yalta (62), Alushta (63),Feodosiia (64), Yevpatoriia (65),
		Ankara (66), Sudak (67), Dzhankoi (68), Chornomorske (69), Kerch (70), Taman (71),Antalya (72), Rome (73),
		Naples (74), Milan (75), Turin (76), Venice (77), Athens (78), Nicosia (79), Limassol (80), Petrozavodsk (81),
		NY (82), Washington (83), Baltimore (84), Izhevsk (85), Kazan (86), Novotroitsk (87), Orenburg (88), Ufa (89),
		Kirov (90), Cherepovets (91), Petropavlovsk (92), Tikhvin (93), Samara (94), Belgorod (95), StaryyOskol(96), Astrakhan (97),
		Arhangelsk (98), Burgas (99)
;
		
	    @Getter
		private int index;
		
		private TypeCityTemperature(int index) {
			this.index=index;
		}

		
		@Override
		public Entity getEntity() {
			return new EntityTemperature (this);
		}
		
		public String getRef () {
			List <String> refs = List.of ("ru/moscow/294021",
			"ru/moscow/294021", "ru/kaluga/293006", "ee/tallinn/127964", "ru/saint-petersburg/295212", "ru/tver/296079",
			"ru/irkutsk/292712", "ee/tartu/131136", "ee/parnu/130058", "ru/yekaterinburg/295863", "ru/lipetsk/293886", "ru/voronezh/296543",
			"ru/rostov-on-don/295146", "ru/krasnodar/293686", "ru/gyelyendzhik/288685", "ru/sochi/293687", "ru/anapa/288689",
			"by/minsk/28580", "ee/johv", "ee/narva/2311089", "ru/tula/295982", "ru/vladimir/296263", 
			"ru/nizhny-novgorod/294199","ru/veliky-novgorod/294457",
			 "ua/kyiv/324505", "ru/nizhny-tagil/290714", "tr/istanbul/318251", "ru/smolensk/295475", "ru/pskov/295031",
			"ru/yaroslavl/296629", "ge/tbilisi/171705", "am/yerevan/16890", "ru/perm/294922", "ru/chelyabinsk/292332",
			"ir/tehran/210841", "jp/tokyo/226396", "ua/simferopol/322464", "ua/sevastopol/6120", "hu/budapest/187423",
			"cz/prague/125594", "at/vienna/31868", "bg/varna/51536", "ro/bucharest/287430", "bg/sofia/51097",
			"me/podgorica/300169", "gb/london/328328", "de/berlin/10178", "de/munich/80331", "fr/paris/623", "fi/helsinki/133328",
			"se/stockholm/314929", "no/oslo/254946", "dk/copenhagen/123094", "lt/vilnius/231459", "lv/riga/225780",
			"ru/maykop/291658", "ru/armavir/293688", "ru/stavropol/295608", "ru/tambov/295902", "ru/volgograd/296363",
			"ru/saratov/295382", "us/philadelphia/19102", "ua/yalta/322329", "ua/alushta/322322", "ua/feodosiia/322325",
			"ua/yevpatoriia/322474", "tr/ankara/316938", "ua/sudak/322321", "ua/dzhankoi/322324",
			"ua/chornomorske/1218365", "ua/kerch/322468", "ru/taman/293663", "tr/antalya/316939",
			"it/rome/213490", "it/naples/212466", "it/milan/214046", "it/turin/214753", "it/venice/216711",
			"gr/athens/182536", "cy/nicosia/124697", "cy/limassol/124034", "ru/petrozavodsk/293141",
			"us/new-york/10007", "us/washington/20006", "us/baltimore/21202", "ru/izhevsk/296181", "ru/kazan/295954",
			"ru/novotroitsk/289747", "ru/orenburg/294535", "ru/ufa/292177",
			"ru/kirov/288509", "ru/cherepovets/296478", "ru/petropavlovsk-kamchatsky", "ru/tikhvin/288958",
			"ru/samara/290396", "ru/belgorod/292195", "ru/staryy-oskol/292196", "ru/astrakhan/292110", 
			"ru/arkhangelsk/292056", "bg/burgas/47424");
			
			return refs.get(index);
		}
		
	}

	@Override
	protected int getLivePeriodMin() {
		return LIVE_PERIOD_MIN;
	}


	
}
