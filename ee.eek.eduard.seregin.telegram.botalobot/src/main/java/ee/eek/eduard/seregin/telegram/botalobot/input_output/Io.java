package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;

/*
 * Every IO_SLEEP_MILLS monitors input from users and notifies every subscribed session of a message from the user 
 * (the user is identified by chatId)
 * Specific input device (Telegram, VKontacte, Console, etc..) is defined in the concrete class
 */

public abstract class Io {

	public final static int IO_SLEEP_MILLIS = 200;
	
	  protected List <Observer> observers;
	  protected List <Observer> newObservers;
	
	  protected Io () {
		this.observers = Collections.synchronizedList(new ArrayList<>()); 
		this.newObservers = Collections.synchronizedList(new ArrayList<>()); 		
	  }

	  public void subscribeFactory (Observer o) {
		  observers.add(o);
	  }
	  
	  public void subscribe (Observer o) {
		 newObservers.add(o);
	  }
	  
	  public void unsubscribe (Observer o) {
		  observers.remove(o);
	  }
	  
	  public void listen () {

		  Thread one = new Thread() {
			   public void run() {
		          listenTread();          
			   }
		  };
		  
		  one.start();
		  try {
		  one.join();
		  } catch (InterruptedException e) {
		  }
	  }		   
	
		
	  private void notifySubscribers (InMessage msg) {
		  observers.stream().forEach(o->o.update(msg));
		  observers.addAll(newObservers);
		  newObservers = new ArrayList<>();
	  }
	  
	  abstract protected InMessage receiveMsg ();

	  private void listenTread() {
	    while (true) {
		    InMessage receivedMsg = receiveMsg ();
		    if (receivedMsg!=null)
	    	notifySubscribers(receivedMsg);       
		  try {
			Thread.sleep(IO_SLEEP_MILLIS);
		  } catch (InterruptedException e) {
			System.out.println("Sleep of io listed has been interrupted");
		  }
		}
	  }
	 

}
	
