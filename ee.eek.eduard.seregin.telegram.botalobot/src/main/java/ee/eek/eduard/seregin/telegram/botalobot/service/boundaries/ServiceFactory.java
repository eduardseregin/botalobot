package ee.eek.eduard.seregin.telegram.botalobot.service.boundaries;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.Observer;
import ee.eek.eduard.seregin.telegram.botalobot.input_output.Session;
import ee.eek.eduard.seregin.telegram.botalobot.service.entities.Entity;
import ee.eek.eduard.seregin.telegram.botalobot.service.strategies.Strategy;

public abstract class ServiceFactory implements Observer {

	protected Session session;

	public ServiceFactory(Session session) {
		this.session = session;
	}
	
	public final void update(InMessage msg) {
		if (isServiceCall (msg)) {
		  session.sendMsg(createEntity().getMessage (msg, getStrategy (msg)));
		}
	}
	
	abstract Strategy getStrategy (InMessage msg);
	abstract Entity createEntity ();
	abstract protected boolean isServiceCall (InMessage msg);
}
