package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage;
import lombok.Getter;

public abstract class Session implements Observer{

  @Getter
  private long chatId;
  protected List <Observer> observers;
 // protected Io device;
  protected Oi sendDevice;
  protected Locale locale;

  public Session (long chatId, Oi sendDevice) {
	this.observers = new ArrayList <> ();
	this.chatId = chatId;
	this.locale=new Locale ("en", "US");
	this.sendDevice=sendDevice;
}

  public void subscribe (Observer o) {
	  observers.add(o);
  }
  
  public void unsubscribe (Observer o) {
	  observers.remove(o);
  }
	
  abstract public void notifySubscribers (InMessage msg);
  abstract public void sendMsg (OutMessage msg);

@Override
public String toString() {
	return "In [chatId=" + chatId + ", observers=" + observers + "]";
}
}

