package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import ee.eek.eduard.seregin.telegram.botalobot.dto.OutMessage;

public class OiConsole extends Oi {

	private static OiConsole oi;
	
	public static OiConsole instance () {
		if (oi == null) oi = new OiConsole ();
		return oi;
	}
	
	private OiConsole () {}
	
	@Override
	void sendMessage(OutMessage msg) {
		System.out.println(msg.getText());
		System.out.println(msg.getKeyboard());

	}

}
