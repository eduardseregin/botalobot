package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import java.util.Date;
import java.util.Scanner;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;

public class IoConsole extends Io {

  public final static Io IO = new IoConsole ();

  private IoConsole() {}

  @Override
  protected InMessage receiveMsg() {
    InMessage msg = new InMessage ();
    msg.setChatId(111111);
    msg.setText(keyboardInput());
	msg.setDate(new Date ());
  return msg;
  }

  @SuppressWarnings("resource") // Close of System.in resource is not working properly
  private String keyboardInput() {
	Scanner fromKey = new Scanner (System.in);
  return fromKey.nextLine();	
  }

}
