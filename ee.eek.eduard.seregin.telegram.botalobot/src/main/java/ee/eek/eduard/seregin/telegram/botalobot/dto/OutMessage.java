package ee.eek.eduard.seregin.telegram.botalobot.dto;

import ee.eek.eduard.seregin.telegram.botalobot.keyboards.KeyBoards.KeyBoard;
import lombok.Data;

@Data
public class OutMessage {

  private long chatId;
  private String text;
  private KeyBoard keyboard;
  private Html isHtml;

public OutMessage(InMessage msg, String text, Html isHtml) {
	this.chatId = msg.getChatId();
	this.text = text;
	this.isHtml = isHtml;
}

public OutMessage(long chatId, String text, Html isHtml) {
	this.chatId = chatId;
	this.text = text;
	this.isHtml = isHtml;
}

  public enum Html {Html, Text}
  
}
