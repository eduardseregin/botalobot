package ee.eek.eduard.seregin.telegram.botalobot.dto;

import java.util.Date;

import lombok.Data;

@Data
public class InMessage {

	private long chatId;
	private Date date;
	private String text;
	
	public InMessage () {}
	public InMessage (String text) {
		this.text=text;
	}
}
