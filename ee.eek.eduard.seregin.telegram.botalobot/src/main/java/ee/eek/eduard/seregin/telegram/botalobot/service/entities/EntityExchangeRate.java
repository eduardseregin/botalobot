package ee.eek.eduard.seregin.telegram.botalobot.service.entities;

import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ExchangeRateServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.service_dtos.ServiceDto;
import ee.eek.eduard.seregin.telegram.botalobot.service.update.ExchangeRateServiceUpdate;

public class EntityExchangeRate extends Entity {

	public final static int LIVE_PERIOD_MIN = 60;
	
	
	protected ExchangeRateServiceDto dto;
	
	
	protected EntityExchangeRate(TypeExchangeRate type) {
		super (type);
		this.dto = new ExchangeRateServiceUpdate().getUpdate();
	}

	@Override
	public ServiceDto getDto() {
		return dto;
	}

	public static enum TypeExchangeRate implements Type {
		CBR;

		@Override
		public Entity getEntity() {
			return new EntityExchangeRate (this);
		}
		
	}

	@Override
	protected int getLivePeriodMin() {
		return LIVE_PERIOD_MIN;
	}


	
}
