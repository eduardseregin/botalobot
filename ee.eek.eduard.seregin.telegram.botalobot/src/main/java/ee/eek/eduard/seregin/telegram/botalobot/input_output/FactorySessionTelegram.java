package ee.eek.eduard.seregin.telegram.botalobot.input_output;

import ee.eek.eduard.seregin.telegram.botalobot.dto.InMessage;
import ee.eek.eduard.seregin.telegram.botalobot.service.boundaries.ServiceExchangeRate;
import ee.eek.eduard.seregin.telegram.botalobot.service.boundaries.ServiceTemperature;

public class FactorySessionTelegram extends FactorySession {

	private static FactorySessionTelegram factory;
	
	public static FactorySession instance (Io io, Oi oi) {
		if (factory == null) factory = new FactorySessionTelegram (io, oi);
		return factory;
	}

	private FactorySessionTelegram(Io io, Oi oi) {
		super(io, oi);
	}
	
	@Override
	Session getSession(InMessage msg) {
		return new SessionStandard (msg.getChatId(), oi);
	}


	@Override
	void subscribeServicesToSession(Session session) {
		session.subscribe(new ServiceTemperature (session));
		session.subscribe(new ServiceExchangeRate (session));
		
	}

}
